<?php
/**
 * Template Name: Registro perfiles
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bridge
 */

get_header(); ?>

<?php
global $wp_query;
$id = $wp_query->get_queried_object_id();

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

$sidebar = $qode_options_proya['category_blog_sidebar'];

$blog_hide_comments = "";
if (isset($qode_options_proya['blog_hide_comments']))
	$blog_hide_comments = $qode_options_proya['blog_hide_comments'];

if(isset($qode_options_proya['blog_page_range']) && $qode_options_proya['blog_page_range'] != ""){
	$blog_page_range = $qode_options_proya['blog_page_range'];
} else{
	$blog_page_range = $wp_query->max_num_pages;
}

?>

	<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
		<script>
		var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
		</script>
	<?php } ?>

		<?php get_template_part( 'title' ); ?>
		<div class="container">
            <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
                <div class="overlapping_content"><div class="overlapping_content_inner">
            <?php } ?>
			<div class="container_inner default_template_holder clearfix">
				<br>
				<h2>Completa tu perfil</h2>
				<br>
				<p>
					Por favor recuerda que los datos que nos vas a proporcionar son importantes para que te podamos ofrecer un servicio seguro y de mejor calidad por lo que te pedimos que llenes los siguientes datos de forma completa y veraz.
				</p>
				<br>
				<form method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="login-form">

							<!-- Datos generales -->
							<div class="row">
								<div class="col-lg-12 col-xs-12">
									<h3>Ficha de identificación </h3>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 col-xs-12">
								    <div class="form-group">
								    	<label for="reg-edad">Edad</label>
								        <input name="reg-edad" type="number" title="" value="" class="form-control" id="reg-edad" placeholder="años" required="required">
								    </div>
								</div>
								<div class="col-lg-6 col-xs-12">
								    <div class="form-group">
								    	<label for="reg-nacimiento">Fecha de nacimiento</label>
								        <input name="reg-nacimiento" type="date" title="" value="" class="form-control" id="reg-nacimiento" placeholder="Edad" required="required">
								    </div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="reg-genero">Género</label>
										<select name="reg-genero" type="text" class="form-control" id="reg_genero">
											<option value="f">Femenino</option>
											<option value="m">Masculino</option>
										</select>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="reg-telefono">Teléfono</label>
								        <input name="reg-telefono" type="text" title="" value="" class="form-control" id="reg-telefono" placeholder="con lada" required="required">
									</div>
								</div>
							</div>

							<!-- Domicilio -->
							<div class="row">
								<div class="col-lg-12 col-xs-12">
									<h3>Domicilio </h3>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-xs-12">
									<div class="form-group">
										<label for="reg-calle">Calle</label>
								        <input name="reg-calle" type="text" title="" value="" class="form-control" id="reg-calle" placeholder="Calle" required="required">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="reg-noext">Número Ext</label>
								        <input name="reg-noext" type="text" title="" value="" class="form-control" id="reg-noext" placeholder="No. Ext." required="required">
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="reg-noint">Número Int</label>
								        <input name="reg-noint" type="text" title="" value="" class="form-control" id="reg-noint" placeholder="No. Int." required="required">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="reg-colonia">Colonia</label>
								        <input name="reg-colonia" type="text" title="" value="" class="form-control" id="reg-colonia" placeholder="Colonia" required="required">
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="reg-cp">Código Postal</label>
								        <input name="reg-cp" type="text" title="" value="" class="form-control" id="reg-cp" placeholder="C.P." required="required">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="reg-municipio">Municipio</label>
								        <input name="reg-municipio" type="text" title="" value="" class="form-control" id="reg-municipio" placeholder="Municipio" required="required">
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="reg-estado">Estado</label>
								        <input name="reg-estado" type="text" title="" value="" class="form-control" id="reg-estado" placeholder="Estado" required="required">
									</div>
								</div>
							</div>

							<!-- Alergias -->
							<div class="row">
								<div class="col-lg-12 col-xs-12">
									<h3>Alergias </h3>
								</div>
								<div class="col-lg-12 col-xs-12" style="display: none;">
									<h4 for="reg-estado">Medicamentos</h4>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Neomicina</legend>
											<input type="radio" name="neomicina" id="neomicina_si" value="1"/>
											<label for="neomicina_si">Si </label>
											<input type="radio" name="neomicina" id="neomicina_no" value="0" checked/>
											<label for="neomicina_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Amikacina, gentamicina u otros aminoglucósidos</legend>
											<input type="radio" name="aminoglucosidos" id="aminoglucosidos_si" value="1"/>
											<label for="aminoglucosidos_si">Si </label>
											<input type="radio" name="aminoglucosidos" id="aminoglucosidos_no" value="0" checked/>
											<label for="aminoglucosidos_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Efectos adversos o alergrias a otros medicamentos</legend>
											<input type="radio" name="otro_medicamento" id="otro_medicamento_no" value="0" checked/>
											<label for="otro_medicamento_no">No </label>

											<input type="radio" name="otro_medicamento" id="otro_medicamento_si" value="1"/>
											<label for="otro_medicamento_si">Si </label>
											<input type="text" name="otro_medicamento_otra" placeholder="Indique">
										</fieldset>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Huevo</legend>
											<input type="radio" name="huevo" id="huevo_si" value="1"/>
											<label for="huevo_si">Si </label>
											<input type="radio" name="huevo" id="huevo_no" value="0" checked/>
											<label for="huevo_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Proteina del pollo</legend>
											<input type="radio" name="proteina_pollo" id="proteina_pollo_si" value="1"/>
											<label for="proteina_pollo_si">Si </label>
											<input type="radio" name="proteina_pollo" id="proteina_pollo_no" value="0" checked/>
											<label for="proteina_pollo_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Gelatina</legend>
											<input type="radio" name="gelatina" id="gelatina_si" value="1"/>
											<label for="gelatina_si">Si </label>
											<input type="radio" name="gelatina" id="gelatina_no" value="0" checked/>
											<label for="gelatina_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Otras alergias</legend>
											<input type="radio" name="otra_alergia" id="otra_alergia_no" value="0" checked/>
											<label for="otra_alergia_no">No </label>

											<input type="radio" name="otra_alergia" id="otra_alergia_si" value="1"/>
											<label for="otra_alergia_si">Si </label>
											<input type="text" name="otra_alergia_otra" placeholder="Indique">
										</fieldset>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>

							<!-- Antecedentes de vacunación -->
							<div class="row">
								<div class="col-lg-12 col-xs-12">
									<h3>Antecedentes de vacunación </h3>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Cuentas con tu cartilla de vacunación</legend>
											<input type="radio" name="cartilla" id="cartilla_si" value="1"/>
											<label for="cartilla_si">Si </label>
											<input type="radio" name="cartilla" id="cartilla_no" value="0" checked/>
											<label for="cartilla_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Si cuentas con cartilla está completa</legend>
											<input type="radio" name="cartilla_completa" id="cartilla_completa_si" value="1"/>
											<label for="cartilla_completa_si">Si </label>
											<input type="radio" name="cartilla_completa" id="cartilla_completa_no" value="0" checked/>
											<label for="cartilla_completa_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Te has aplicado alguna vacuna en los últimos dos años</legend>
											<input type="radio" name="vacuna_ultimos_anios" id="vacuna_ultimos_anios_si" value="1"/>
											<label for="vacuna_ultimos_anios_si">Si </label>
											<input type="radio" name="vacuna_ultimos_anios" id="vacuna_ultimos_anios_no" value="0" checked/>
											<label for="vacuna_ultimos_anios_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Te has aplicado el esquema completo de varicela</legend>
											<input type="radio" name="varicela_completo" id="varicela_completo_si" value="1"/>
											<label for="varicela_completo_si">Si </label>
											<input type="radio" name="varicela_completo" id="varicela_completo_no" value="0" checked/>
											<label for="varicela_completo_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-lg-12 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Has presentado efectos adversos o alergias a la aplicación de alguna vacuna</legend>
											<input type="radio" name="vacuna_adverso" id="vacuna_adverso_no" value="0" checked/>
											<label for="vacuna_adverso_no">No </label>
											<input type="radio" name="vacuna_adverso" id="vacuna_adverso_si" value="1"/>
											<label for="vacuna_adverso_si">Si </label>
											<input type="text" name="vacuna_adverso_texto" placeholder="Indique a cual(es)">
										</fieldset>
									</div>
								</div>
							</div>

							<!-- Enfermedades -->
							<div class="row">
								<div class="col-lg-12 col-xs-12">
									<h3>Enfermedades </h3>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Arritmias cardiacas o problemas en la conducción cardiaca</legend>
											<input type="radio" name="arritmia" id="arritmia_si" value="1"/>
											<label for="arritmia_si">Si </label>
											<input type="radio" name="arritmia" id="arritmia_no" value="0" checked/>
											<label for="arritmia_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Tienes puesto un marcapasos</legend>
											<input type="radio" name="marcapasos" id="marcapasos_si" value="1"/>
											<label for="marcapasos_si">Si </label>
											<input type="radio" name="marcapasos" id="marcapasos_no" value="0" checked/>
											<label for="marcapasos_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Padeces otras enfermedades del sistema cardiovascular</legend>
											<input type="radio" name="enfermedad_cardiovascular" id="enfermedad_cardiovascular_no" value="0" checked/>
											<label for="enfermedad_cardiovascular_no">No</label>
											<input type="radio" name="enfermedad_cardiovascular" id="enfermedad_cardiovascular_si" value="1"/>
											<label for="enfermedad_cardiovascular_si">Si </label>
											<input type="text" name="enfermedad_cardiovascular_texto" placeholder="Indique a cual(es)">
										</fieldset>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Padeces o has padecido algún tipo de anemia hemolítica</legend>
											<input type="radio" name="anemia_hemolitica" id="anemia_hemolitica_si" value="1"/>
											<label for="anemia_hemolitica_si">Si </label>
											<input type="radio" name="anemia_hemolitica" id="anemia_hemolitica_no" value="0" checked/>
											<label for="anemia_hemolitica_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Has padecido eritema multideforme, síndrome de Stevens-Johnson o necrólisis epedérmica tóxica</legend>
											<input type="radio" name="eritema" id="eritema_si" value="1"/>
											<label for="eritema_si">Si </label>
											<input type="radio" name="eritema" id="eritema_no" value="0" checked/>
											<label for="eritema_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Has padecido varicela</legend>
											<input type="radio" name="varicela" id="varicela_si" value="1"/>
											<label for="varicela_si">Si </label>
											<input type="radio" name="varicela" id="varicela_no" value="0" checked/>
											<label for="varicela_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Padeces fenilcetonuria</legend>
											<input type="radio" name="fenilcetonuria" id="fenilcetonuria_si" value="1"/>
											<label for="fenilcetonuria_si">Si </label>
											<input type="radio" name="fenilcetonuria" id="fenilcetonuria_no" value="0" checked/>
											<label for="fenilcetonuria_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Padeces enfermedades respiratorias</legend>
											<input type="radio" name="enfermedad_respiratoria" id="enfermedad_respiratoria_no" value="0" checked/>
											<label for="enfermedad_respiratoria_no">No</label>
											<input type="radio" name="enfermedad_respiratoria" id="enfermedad_respiratoria_si" value="1"/>
											<label for="enfermedad_respiratoria_si">Si </label>
											<input type="text" name="enfermedad_respiratoria_texto" placeholder="Indique a cual(es)">
										</fieldset>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Padeces enfermedades renales (riñón)</legend>
											<input type="radio" name="enfermedad_renal" id="enfermedad_renal_no" value="0" checked/>
											<label for="enfermedad_renal_no">No</label>
											<input type="radio" name="enfermedad_renal" id="enfermedad_renal_si" value="1"/>
											<label for="enfermedad_renal_si">Si </label>
											<input type="text" name="enfermedad_renal_texto" placeholder="Indique a cual(es)">
										</fieldset>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Padeces enfermedades hepáticas (higado)</legend>
											<input type="radio" name="enfermedad_hepatica" id="enfermedad_hepatica_no" value="0" checked/>
											<label for="enfermedad_hepatica_no">No</label>
											<input type="radio" name="enfermedad_hepatica" id="enfermedad_hepatica_si" value="1"/>
											<label for="enfermedad_hepatica_si">Si </label>
											<input type="text" name="enfermedad_hepatica_texto" placeholder="Indique a cual(es)">
										</fieldset>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Has padecido el síndrome de Guillain-Barré</legend>
											<input type="radio" name="guillan-barre" id="guillan-barre_si" value="1"/>
											<label for="guillan-barre_si">Si </label>
											<input type="radio" name="guillan-barre" id="guillan-barre_no" value="0" checked/>
											<label for="guillan-barre_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Padeces otras enfermedades neurológicas</legend>
											<input type="radio" name="enfermedad_neurologica" id="enfermedad_neurologica_no" value="0" checked/>
											<label for="enfermedad_neurologica_no">No</label>
											<input type="radio" name="enfermedad_neurologica" id="enfermedad_neurologica_si" value="1"/>
											<label for="enfermedad_neurologica_si">Si </label>
											<input type="text" name="enfermedad_neurologica_texto" placeholder="Indique a cual(es)">
										</fieldset>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Padeces depresión</legend>
											<input type="radio" name="depresion" id="depresion_si" value="1"/>
											<label for="depresion_si">Si </label>
											<input type="radio" name="depresion" id="depresion_no" value="0" checked/>
											<label for="depresion_no">No</label>
										</fieldset>
									</div>
								</div>

								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Padeces ansiedad</legend>
											<input type="radio" name="ansiedad" id="ansiedad_si" value="1"/>
											<label for="ansiedad_si">Si </label>
											<input type="radio" name="ansiedad" id="ansiedad_no" value="0" checked/>
											<label for="ansiedad_no">No</label>
										</fieldset>
									</div>
								</div>

								<div class="clearfix"></div>

								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Padeces ezquizofrenia</legend>
											<input type="radio" name="ezquizofrenia" id="ezquizofrenia_si" value="1"/>
											<label for="ezquizofrenia_si">Si </label>
											<input type="radio" name="ezquizofrenia" id="ezquizofrenia_no" value="0" checked/>
											<label for="ezquizofrenia_no">No</label>
										</fieldset>
									</div>
								</div>

								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Padeces ataques de pánico</legend>
											<input type="radio" name="ataques_panico" id="ataques_panico_si" value="1"/>
											<label for="ataques_panico_si">Si </label>
											<input type="radio" name="ataques_panico" id="ataques_panico_no" value="0" checked/>
											<label for="ataques_panico_no">No</label>
										</fieldset>
									</div>
								</div>

								<div class="clearfix"></div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Trastorno bipolar I o II</legend>
											<input type="radio" name="trastorno_bipolar" id="trastorno_bipolar_si" value="1"/>
											<label for="trastorno_bipolar_si">Si </label>
											<input type="radio" name="trastorno_bipolar" id="trastorno_bipolar_no" value="0" checked/>
											<label for="trastorno_bipolar_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Otros trastornos psiquiátricos</legend>
											<input type="radio" name="trastorno_psiquiatrico" id="trastorno_psiquiatrico_no" value="0" checked/>
											<label for="trastorno_psiquiatrico_no">No</label>
											<input type="radio" name="trastorno_psiquiatrico" id="trastorno_psiquiatrico_si" value="1"/>
											<label for="trastorno_psiquiatrico_si">Si </label>
											<input type="text" name="trastorno_psiquiatrico_texto" placeholder="Indique a cual(es)">
										</fieldset>
									</div>
								</div>

								<div class="clearfix"></div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Padeces VIH/SIDA</legend>
											<input type="radio" name="vih" id="vih_si" value="1"/>
											<label for="vih_si">Si </label>
											<input type="radio" name="vih" id="vih_no" value="0" checked/>
											<label for="vih_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Padeces Cancer</legend>
											<input type="radio" name="cancer" id="cancer_si" value="1"/>
											<label for="cancer_si">Si </label>
											<input type="radio" name="cancer" id="cancer_no" value="0" checked/>
											<label for="cancer_no">No</label>
										</fieldset>
									</div>
								</div>

								<div class="clearfix"></div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Padeces otras enfermedades</legend>
											<input type="radio" name="otras_enfermedades" id="otras_enfermedades_no" value="0" checked/>
											<label for="otras_enfermedades_no">No</label>
											<input type="radio" name="otras_enfermedades" id="otras_enfermedades_si" value="1"/>
											<label for="otras_enfermedades_si">Si </label>
											<input type="text" name="otras_enfermedades_texto" placeholder="Indique a cual(es)">
										</fieldset>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Usas corticoesteroides u otros inmunosupresores</legend>
											<input type="radio" name="inmunosupresores" id="inmunosupresores_si" value="1"/>
											<label for="inmunosupresores_si">Si </label>
											<input type="radio" name="inmunosupresores" id="inmunosupresores_no" value="0" checked/>
											<label for="inmunosupresores_no">No</label>
										</fieldset>
									</div>
								</div>

								<div class="clearfix"></div>

								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Estas usando quimioterapia/radioterapia</legend>
											<input type="radio" name="quimioterapia" id="quimioterapia_si" value="1"/>
											<label for="quimioterapia_si">Si </label>
											<input type="radio" name="quimioterapia" id="quimioterapia_no" value="0" checked/>
											<label for="quimioterapia_no">No</label>
										</fieldset>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Usas otros medicamentos </legend>
											<input type="radio" name="otros_medicamentos" id="otros_medicamentos_no" value="0" checked/>
											<label for="otros_medicamentos_no">No</label>
											<input type="radio" name="otros_medicamentos" id="otros_medicamentos_si" value="1"/>
											<label for="otros_medicamentos_si">Si </label>
											<input type="text" name="otros_medicamentos_texto" placeholder="Indique a cual(es)">
										</fieldset>
									</div>
								</div>
							</div>

							<!-- Ginecológicos -->
							<div class="row">
								<div class="col-lg-12 col-xs-12">
									<h3>Ginecológicos (en caso de ser mujer y en edad fértil, 12 a 25 años) </h3>
								</div>
								<div class="col-lg-12 col-xs-12">
									<div class="form-group">
										<fieldset>
											<legend>Embarazada actualmente </legend>
											<input type="radio" name="embarazada" id="embarazada_no" value="0" checked/>
											<label for="embarazada_no">No</label>
											<input type="radio" name="embarazada" id="embarazada_si" value="1"/>
											<label for="embarazada_si">Si </label>
											<input type="text" name="embarazada_texto" placeholder="Cuantas semanas">
										</fieldset>
									</div>
							</div>
						</div>
					</div>
					<br>
				</form>
			</div>
            <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
                </div></div>
            <?php } ?>
		</div>
<?php get_footer(); ?>